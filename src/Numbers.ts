import { TypeTests } from "./TypeTests";

export class Numbers {

	public static bigIntToLongHex( b: bigint ): string {
        return `0x${Numbers.dec2hex( b )}`;
	}

	public static intToLongHex( i: number ): string {
		return Numbers.intPairToLongHex( [ 0, i ] );
	}

	public static intPairToLongHex( a: [number, number] ): string {
		return `0x${a.map(Numbers.dec2hex).join("")}`;
	}

	public static intToShortHex( i: number ): string {
		return `00000000${i.toString( 16 )}`.substr( -8 );
	}

	public static dec2hex( d: number|bigint ): string|undefined {
		switch(TypeTests.getType(d)) {
			case "BigInt":
				return `0000000000000000${d.toString( 16 )}`.substr(-16);
			case "Number":
				return `00000000${d.toString( 16 )}`.substr( -8 );
			default:
				return undefined;
		}
	}

}