import NodeFetch from "node-fetch";
import NodeFormData from "form-data";
const fetch = typeof window === "undefined" ? NodeFetch : window.fetch;
const FormData = typeof window === "undefined" ? NodeFormData : window.FormData;

export { fetch, FormData };