interface Config {
    logger: Console;
    debug: boolean;
}

export const Config: Config = {
    logger: console,
    debug: false,
};