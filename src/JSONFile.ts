"use strict";

/** @module JSONFile */

import FS from "fs";
import Path from "path";
import { TypeTests } from "./TypeTests";


interface JSONFileOptions {
	mode?: number,
};

export class JSONFile {

	static fetch ( filePath:string, options: { mode?: number } ) { return fetchFile( filePath, options ); }
	
	static new( filePath:string, options: { mode?: number } ) { return fetchFile( filePath, options ); }

}

async function fetchFile(filePath:string, options: JSONFileOptions ) {

	if(!filePath) { return null; }

	let writeInProgress: Promise<void> | undefined = undefined;

	const handlers: ProxyHandler<{ [ key: string ]: any }> = {
		deleteProperty( target: { [ key: string ]: any }, p: string ): boolean {
			delete target[p];
			writeToFile();
			return true;
		},
		set( target: { [ key: string ]: any }, prop: string, value: any, /*receiver*/): boolean {
			target[prop] = TypeTests.isObject(value) ? proxify(value) : value ;
			writeToFile();
			return true;
		},
	};

	let obj = {};
	try {
		const json = await FS.promises.readFile( filePath, { encoding: "utf-8" } );
		obj = JSON.parse( json );
	} catch( err ) {
		if(( err as NodeJS.ErrnoException ).code=="ENOENT") { // If the file hasn't been
			await writeToFile();
		} else {
			throw err;
		}
	}

	return proxify(obj);

	function proxify( o: { [ key: string ]: any } ): { [ key: string ]: any } {
		for(let key in o) {
			if( TypeTests.isObject( o[key] ) ) {
				o[key] = proxify(o[key]);
			}
		}

		return new Proxy(o, handlers);
	}

	async function writeToFile() {

		const mode = options && options.mode || 0o644;

		const writeFileOptions: FS.WriteFileOptions = {
			encoding : "utf-8",
			mode     : mode,
		};

		const mkdirOptions: FS.MakeDirectoryOptions = {
			mode      : mode | ( mode >> 2 & 0o111 ), // execute bit matches read bit
			recursive : true,
		};

		try {
			await FS.promises.mkdir( Path.dirname(filePath), mkdirOptions );
		} catch(err) {
			if(( err as NodeJS.ErrnoException ).code=="EEXIST") {
				/*IGNORE IF FOLDER ALREADY EXISTS*/
			} else {
				throw err;
			}
		}

		while( writeInProgress ) { await writeInProgress; }
		await ( writeInProgress = FS.promises.writeFile( filePath, JSON.stringify(obj, null, 2)+"\n", writeFileOptions ) );
		writeInProgress = undefined;
		return true;
	}
}