export type RejectFunction = ( reason?: any ) => void;
export type ResolveFunction<T = void|undefined> = ( value: T | PromiseLike<T> ) => void;