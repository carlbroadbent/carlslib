const ESCAPE_SEQUENCE_REGEX: RegExp = /\\(?:([bfnrtv])|([0-8]{2})|x([0-9a-f]{2})|u([0-9a-f]{4})|U([0-9a-f]{8})|(.))/g;

interface Lookup {
	[name: string]: string,
}
const ESCAPE_MAPPINGS: Lookup = {
	b: "\x08",
	f: "\x0c",
	n: "\x0a",
	r: "\x0d",
	t: "\x09",
	v: "\x0b",
};
const ESCAPE_MAPPINGS_REV: Lookup = Object.entries(ESCAPE_MAPPINGS).reduce( (R, [k,v]) => ( { ...R, [v]:k, } ), {} );

class Strings {

	static appendSpacesToLength(str: string, length: number): string {
		if(str.length>length) {
			throw new Error(`Can't make string of length ${str.length} ${length} characters long by adding more spaces.`);
		}
		return str + " ".repeat(length - str.length);
	}

	static parseCommandLineString( str: string ): Array<string> {
		let escape: boolean = false;
		let inQuotes: String | null = null;
		const parts = str.split("").reduce( ( R, c, ) => {

			if( "'\"".split("").includes(c) ) {
				if(inQuotes===c) {
					if( escape ) {
						R[R.length-1]+=c;
					} else {
						inQuotes = null;
					}
				} else {
					inQuotes = c;
				}
			} else if( c==="\\" && process.platform!=="win32" ) {
				if( escape ) { R[R.length-1]+=c; }
			} else if( c===" " && !inQuotes && !escape ) {
				R.push( "" );
			} else {
				R[R.length-1]+=c;
			}

			escape = ( c==="\\" && escape===false && process.platform!=="win32" );
			return R;
		}, [""] );
		if(escape) { throw Error( `Command line string ended with escape character: ${str}`); }
		if(inQuotes) { throw Error( `Command line string not quoted properly: ${str}`); }
		return parts;
	}

	static prependSpacesToLength(str: string, length: number) {
		if(str.length>length) {
			throw new Error(`Can't make string of length ${str.length} ${length} characters long by adding more spaces.`);
		}
		return " ".repeat(length - str.length) + str;
	}

	static toAscii( s: string ) {
		return s.split("").map(unicodeEscapeChar).join("");
	}

	static toUnicode( s: string ): string {
		// @ts-ignore
		return s.replace(ESCAPE_SEQUENCE_REGEX, (a:any, b:string, c:string, d:string, e:string, f:string, g:string) => {
			if(b) { return ESCAPE_MAPPINGS[b]; }
			if(c) { return String.fromCharCode(parseInt(c, 8)); }
			if(d||e||f) { return String.fromCharCode(parseInt(d||e||f, 16) ); }
			return g;
		} )
		;
	}
}

export { Strings };

/**
 *
 * @param {String} c
 */
function unicodeEscapeChar( c: string ) {
	if( ESCAPE_MAPPINGS_REV.hasOwnProperty(c)===true)  { return `\\${ESCAPE_MAPPINGS_REV[c]}`; }
	const n = c.charCodeAt(0);
	if(n<0x20)    { return `\\${n.toString(8)}`; }
	if(n<0x80)    { return c; }
	if(n<0x100)   { return `\\x${zeroPadHex(n, 2)}`; }
	if(n<0x10000) { return `\\u${zeroPadHex(n, 4)}`; }
	return `\\U${zeroPadHex(n, 8)}`;
}

/**
 * Generate a zero-padded hexadecimal string representative of a number.
 * @param {Number} n number to be converted
 * @param {Number} l character length of resultant string
 */
function zeroPadHex(n:number, l:number) {
	return ( "0".repeat(l)+n.toString(16) ).slice(-l);
}