import Crypto from "crypto";
import FS from "fs";
import { fName, FormattedLogger, TypeTests } from ".";
import { RejectFunction, ResolveFunction } from "./types";
// import { fName } from "./Functions/fName";
// import { FormattedLogger } from "./FormattedLogger";
// import { TypeTests } from "./TypeTests";

class Hasher {

	/**
	 * Produce a hash/hashes for a file's data.
	 *
	 * @param {String} path path to file for hashing
	 * @param {String|Array<String>} algs hashing algorithms to use. If Array is provided,
	 * 								an object with the algorithm names as keys will be returned.
	 *
	 * @returns {Promise<String|Object.<String,String>>} Promise resolving a String representation of the hash,
	 * 								or an object with algorithm name as the keys and hash string as the values.
	 */

	public static async hashFile(path: string, algs: string): Promise<string>;
	public static async hashFile(path: string, algs: string[] ): Promise<{[key:string]: string}>;
	public static async hashFile(path: string, algs: string|string[] ): Promise<string|{[key:string]: string}> {
		FormattedLogger.debug(`${fName(true, 1)}(${[...arguments].map(()=>"%j").join(", ")})`, ...arguments);
		TypeTests.validateType(path, "String");
		TypeTests.validateType(algs, ["Array","String"]);
		if(Array.isArray(algs) && algs.some( s => !TypeTests.isType(s, "String") ) ) {
			throw new Error("In Hasher.hashFile, algs must be a String or String[]");
		}
		if( Array.isArray(algs) ) {
			return Hasher.hashStream( FS.createReadStream( path, { flags: "r", autoClose: true, } ), algs );
		} else {
			return Hasher.hashStream( FS.createReadStream( path, { flags: "r", autoClose: true, } ), algs );
		}
	}

	/**
	 * Produce a hash/hashes for a stream's data.
	 *
	 * @param {Stream.Readable} readStream Stream to hash
	 * @param {String|Array<String>} algs hashing algorithms to use. If Array is provided,
	 * 								an object with the algorithm names as keys will be returned.
	 *
	 * @returns {Promise<String|Object.<String,String>>} Promise resolving a String representation of the hash,
	 * 								or an object with algorithm name as the keys and hash string as the values.
	 */
	public static async hashStream(readStream: NodeJS.ReadableStream, algs: string): Promise<string>;
	public static async hashStream(readStream: NodeJS.ReadableStream, algs: string[] ): Promise<{[key:string]: string}>;
	public static async hashStream(readStream: NodeJS.ReadableStream, algs: string|string[] ): Promise<string|{[key:string]: string}> {
		if(!TypeTests.isReadableStream(readStream)) {
			throw new Error("In Hasher.hashStream, readStream must be a ReadableStream");
		}
		TypeTests.validateType(algs, ["Array","String"]);
		if(Array.isArray(algs) && algs.some( s => !TypeTests.isType(s, "String") ) ) {
			throw new Error("In Hasher.hashStream, algs must be a String or String[]");
		}
		if(Array.isArray(algs) ) {
			const hashers: {[key:string]: Crypto.Hash}  = algs.reduce( (R, alg) => { R[alg]=Crypto.createHash(alg); return R; }, {} as {[key:string]: Crypto.Hash} );
			// @ts-ignore
			readStream.on("data", chunk => Object.values(hashers).forEach( hasher => hasher.update(chunk, "binary") ) );
			return new Promise( (resolve: ResolveFunction<{[key:string]: string}>, reject: RejectFunction) => {
				readStream.on( "end", err => err?reject(err):resolve( algs.reduce( (R, alg) => ( {...R, [alg]:hashers[alg].digest("hex"), } ), {} ) ) );
				readStream.on( "error", reject );
			} );
		} else {
			return (await this.hashStream( readStream, [algs] ) )[algs];
		}
	}

	/**
	 * Produce a hash/hashes for a file's data.
	 *
	 * @param {String} str string data to generate hash for
	 * @param {String|Array<String>} algs hashing algorithms to use. If Array is provided,
	 * 								an object with the algorithm names as keys will be returned.
	 *
	 * @returns {String|Object.<String,String>} Promise resolving a String representation of the hash,
	 * 								or an object with algorithm name as the keys and hash string as the values.
	 */
	public static hashString(str: string, algs: string): string;
	public static hashString(str: string, algs: string[] ): {[key:string]: string};
	public static hashString(str: string, algs: string|string[] ): string|{[key:string]: string} {
		TypeTests.validateType(str, "String");
		TypeTests.validateType(algs, ["Array","String"]);
		if(Array.isArray(algs) && algs.some( s => !TypeTests.isString( s ) ) ) {
			throw new Error("In Hasher.hashString, algs must be a String or String[]");
		}
		if( Array.isArray(algs) ) { 
			const hashers: {[key:string]: Crypto.Hash}  = algs.reduce( (R, alg) => { R[alg]=Crypto.createHash(alg); return R; }, {} as {[key:string]: Crypto.Hash} );
			// @ts-ignore
			Object.values(hashers).forEach( hasher => hasher.update(str, "binary") );
			return algs.reduce( (R, alg) => ( {...R, [alg]:hashers[alg].digest("hex"), } ), {} );
		} else {
			return (this.hashString( str, [algs] ) )[algs];
		}
	}
}

export { Hasher };
