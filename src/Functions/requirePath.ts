import { fName } from "./fName";
import { Logging } from "../Logging";

/**
 * Set new path for require to use when looking for files to import.
 *
 * @param {String} newPath new folder path to use.
 */
export function requirePath(newPath: string) {
	Logging.debug( `${fName(true)}( %j )`, newPath );
	process.env.NODE_PATH = [ ...( ( process.env.NODE_PATH || "" ).split(":") ), newPath ].join(":");
	require( "module" ).Module._initPaths();
}