import { getCodeLocation } from "./getCodeLocation";

/**
 * Gets the function name of the calling function.
 *
 * @function fName
 * @param {Boolean} includeClassName Should attach the class name to the function name.
 *                             This may return the file name if the class name
 *                             cannot be determined correctly.
 * @param {Number} [lookback=0] How many functions back in the stack should we be inspecting?
 *                             The default is the calling function, a value of one would
 *                             look at the next function in the stack.
 */
export function fName(includeClassName?:boolean, lookback:number = 0 ): string {
	const { className, functionName, file } = getCodeLocation(lookback+1);
	if( !!includeClassName) {
		if( !!className ) {
			return `${className}.${functionName}`;
		}
		if( file!==undefined && !["REPL", "REPL2"].includes(file) ) {
			const filePrefix = (file.split("/").pop() as string).split(".").shift()
			return `${filePrefix}.${functionName}`
		}
	}
	return functionName || "";
}