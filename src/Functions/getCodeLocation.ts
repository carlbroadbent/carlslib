/**
 * @typedef ErrorLocation
 * @member {string} [className] class where the function is defined
 * @member {string} [file] path to file
 * @member {string} [functionName] name of the function, undefined if none exists
 * @member {number} line line in file
 * @member {column} column column in file
 */
export type ErrorLocation = {
	className?: string,
	file?: string,
	functionName?: string,
	line: number,
	column: number,
};

/**
 * Gets the location in file of a fucntion from the runtime stack.
 *
 * @function getCodeLocation
 * @param {Number} [lookBack=0] How many functions back in the stack should we be inspecting?
 *                             The default is the calling function, a value of one would
 *                             look at the next function in the stack.
 */
export function getCodeLocation( lookback: number = 0 ): ErrorLocation {
	const stack: string = Error().stack as string;
	// DebugLogger.debug(stack);
	const lineString = stack.split("\n")[2+lookback].trim();
	const lineParts = lineString.split(" ");
	const suffix:string = lineParts.pop() as string;

	let className: string|undefined;
	let functionName: string|undefined;

	if(lineParts.length===2) {
		const fParts = (lineParts.pop() as string).split(".");
		functionName = fParts.pop();
		className = fParts.shift();
	} else if(lineParts.length===4 && lineParts[2]==="[as") {
		let func = (lineParts.pop() as string).slice(0,-1);
		let fParts = lineParts[1].split(".")
		if(fParts[fParts.length-1]==="<computed>") {
			fParts.pop();
			fParts.push(func);
			if(fParts[0]==="Function") {
				fParts.shift();
			}
			func = fParts.join(".");
		}
		fParts = func.split(".");
		functionName = fParts.pop();
		className = fParts.shift();
	}
	
	const fileString : string = suffix.endsWith(")") ? suffix.substr(1, suffix.length-2) : suffix;
	const fileParts = fileString.split(":") as string[];

	if(functionName===undefined) functionName = fileParts[0];

	const file = ["REPL", "REPL2"].includes(fileParts[0]) ? undefined : fileParts[0];
	const line = Number(fileParts[1]);
	const column = Number(fileParts[2]);

	if( className==="Function" ) {
		className = undefined;
	}
	
	return {className, file, functionName, line, column };
}