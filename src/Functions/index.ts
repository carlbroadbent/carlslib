import { TypeTests } from "../TypeTests";
import { ResolveFunction } from "../types";

export * from "./execute";
export * from "./fName";
export * from "./getCodeLocation";
export * from "./requirePath";
export * from "./unicode";

/**
 * Copy functions from one object to another.
 *
 * @function cloneFunctions
 * @param {Object} sourceObj object to copy functions from
 * @param {Object} targetObj object to copy functions to
 * @returns {Object} the target object with the new functions set
 */

export function cloneFunctions(sourceObj: object, targetObj: { [ key: string ]: any } ): object {
	Object.entries(sourceObj)
		.forEach( ( [k, v] ) => {
			if( [ "length", "prototype", "name", ].includes(k)===false && TypeTests.getType(v) === "Function" ) {
				targetObj[k] = v;
			}
		} );
	return targetObj;
}

/**
 * Lists the names functions in an object.
 *
 * @function getFunctionNames
 * @param {Object} o the object to list functions of
 * @returns {string[]} a list of object parameter names for the functions in the object.
 */

export function getFunctionNames(o:object): Array<string> {
	return Object.getOwnPropertyNames(o)
	.filter( s => [ "length", "prototype", "name", ].includes(s)===false );
}

/**
 * Tests whether code is running in Electron environment.
 *
 * @function isElectron
 * @returns {Boolean} true if running in Electron, else false
 */

export function isElectron(): boolean { return !!process.versions.electron; }


/**
 * Return a promise that resolves after the specified duration in milliseconds.
 *
 * @async
 * @function sleep
 * @param {Number} durationMs number of milliseconds to pause for.
 * @returns {Promise} A promise that will resolve undefined after the provided time.
 */
export async function sleep(durationMs: number): Promise<void> {
	return new Promise<void>( ( resolve: ResolveFunction<void> ) => setTimeout( resolve, durationMs ) );
}