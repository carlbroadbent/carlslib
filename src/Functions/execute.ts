"use strict";

import { ChildProcess, spawn, SpawnOptions, } from "child_process";
import OS from "os";
import { Strings } from "../Strings";
import { RejectFunction, ResolveFunction } from "../types";

const {parseCommandLineString} = Strings;

/**
 * Runs a command asynchronously, returning a promise of stdout as a string.
 *
 * @async
 * @function execute
 * @param {String} command command line string to execute
 * @param {Boolean} [detached=false] true if the command should run detached
 *
 * @returns {Promise.<String, Error>} A Promise that returns the executed command's stdout as a string.
 * If there was an error executing the command, an error with the exit code, the command, stdout and stderr output is rejected.
 */

export async function execute( command:string, detached:boolean=false ): Promise<boolean|string> {
	const parts = parseCommandLineString(command);
	// console.log(parts);

	if(process.platform==="win32") { parts.unshift("cmd.exe", "/c"); }

	const cmd = parts.shift();
	if(!cmd) {
		throw Error("no command was supplied");
	}


	const stdio: any = detached?"ignore":"pipe";
	const spawnOpts: SpawnOptions = {
		cwd: OS.homedir(),
		detached,
		stdio,
	};

	console.log("spawnOpts: %j", spawnOpts);

	const child:ChildProcess = spawn( cmd, parts, spawnOpts );

	if(detached) {
		child.unref();
		return true;
	} else {
		child.stdin?.end(`${command}\nexit\n`);
		const buffers = { err:[] as Array<any>, out:[] as Array<any>, };
		Object.entries(buffers).forEach( ( [ k, v, ] ) => {
			const readable: NodeJS.ReadableStream = (k==="err"?child.stderr:child.stdout) as NodeJS.ReadableStream;
			readable.on( "data", (chunk: Array<any>) => v.push(...chunk) ); // child.std(err|out).on("data")
		} );
		return new Promise<string>( ( resolve: ResolveFunction<string>, reject: RejectFunction ) => {
			child.on( "close", (code: number) => {
				if(code) {
					const out = Buffer.from(buffers.out).toString();
					const err = Buffer.from(buffers.err).toString();
					return reject( Error( JSON.stringify( { code, command, out, err, } ) ) );
				}
				Object.entries(buffers).reduce( ( R, [k,v], ) => ({...R, [k]: Buffer.from(v).toString()}), {} );
				return resolve( Buffer.from(buffers.out).toString() );
			} );
		} );
	}
}