import { Strings } from "../Strings";
import { TypeTests } from "../TypeTests";

export const unicodeEscape = generateTransform( Strings.toAscii );
export const unicodeUnescape = generateTransform( Strings.toUnicode );

function generateTransform( transformString: Function ) {
	return function transformObject(obj: any ): any {
		switch( TypeTests.getType(obj) ) {
			case "Array" : return obj.map( transformObject );
			case "Object":
				return Object.entries(obj).reduce( (R, [ k, v ] ) => ( {...R, [k]: transformObject(v), }) , {} );
			case "String": return transformString(obj as string);
			default      : return obj;
		}
	};
}