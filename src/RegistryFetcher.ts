"use strict";
import WinReg, {Registry, RegistryItem} from "winreg";
import { RejectFunction, ResolveFunction } from "./types";

export interface RegistryFetcherItem {
	name: string;
	value: RegistryFetcherItemValue;
}

interface RegistryFetcherKeyItem extends RegistryFetcherItem {
	value: RegistryFetcherKeyItemValue;
}

interface RegistryFetcherKeyItemDeep extends RegistryFetcherKeyItem {
	value: RegistryFetcherKeyItemValueDeep;
}

interface RegistryFetcherKeyItemShallow extends RegistryFetcherKeyItem {
	value: RegistryFetcherKeyItemValueShallow;
}

interface RegistryFetcherValueItem extends RegistryFetcherItem {
	value: RegistryFetcherValueItemValue;
}

export interface RegistryFetcherItemValue {
	configurable: boolean;
	enumerable: boolean;
	value: RegistryValue;
	writable: boolean;
}

interface RegistryFetcherKeyItemValue extends RegistryFetcherItemValue {
	value: RegistryKeyValue;
}

interface RegistryFetcherKeyItemValueDeep extends RegistryFetcherKeyItemValue {
	value: RegistryKeyValueDeep;
}

interface RegistryFetcherKeyItemValueShallow extends RegistryFetcherKeyItemValue {
	value: RegistryKeyValueShallow;
}

interface RegistryFetcherValueItemValue extends RegistryFetcherItemValue {
	value: RegistryValueValue;
}

export type RegistryValue = RegistryKeyValue | RegistryValueValue;
type RegistryKeyValue = RegistryKeyValueObject | RegistryKeyValueAsyncFunction;

type RegistryKeyValueAsyncFunction = () => Promise<RegistryKeyValueObject>;
type RegistryKeyValueObject = RegistryKeyValueDeep | RegistryKeyValueShallow

type RegistryKeyValueDeep = { [key:string]: RegistryValue };
type RegistryKeyValueShallow = { [key:string]: RegistryValueValue | RegistryKeyValueAsyncFunction };
type RegistryValueValue = number | string | string[];


export class RegistryFetcher {

	static async getDeep(key: string): Promise<object|undefined> {
		if(process.platform!=="win32") { return undefined; }
		return getKeyAsObject(key, true);
	}

	public static async get( key: string ): Promise< RegistryKeyValueObject | undefined >
	public static async get( key: string, propNames: string | string[] ): Promise< | RegistryValue | undefined >
	public static async get( key: string, propNames?: string | string[] | undefined ): Promise< RegistryValue | undefined >
	public static async get( key: string, propNames?: string | string[]	 ): Promise< RegistryValue | undefined > {
		if( process.platform !== "win32" ) { return undefined; }
		if( typeof key !== "string" ) { return undefined; }
		try {
			let allValues: RegistryKeyValueShallow = ( await getKeyAsObject(key, false) );
			if(propNames === undefined ) { return allValues; }
			if(typeof propNames === "string" && allValues.hasOwnProperty(propNames) ) { return allValues[propNames] as RegistryKeyValueAsyncFunction; }
			if(Array.isArray(propNames)) {
				const result: RegistryKeyValueObject = {};
				for(let name of propNames) {
					if( Object.prototype.hasOwnProperty.call(allValues, name) ) {
						result[name] = allValues[name];
					}
				}
				return result;
			}
		} catch(err) {
			console.error(err);
			return undefined;
		}
		return undefined;
	}

	public static async getOrUndefined( key: string ): Promise< RegistryValue | undefined >
	public static async getOrUndefined( key: string, propNames: string | string[] ): Promise< RegistryValue | undefined >
	public static async getOrUndefined( key: string, propNames?: string | string[] | undefined ): Promise< RegistryValue | undefined >
	public static async getOrUndefined( key: string, propNames?: string | string[] ): Promise< RegistryValue | undefined > {
		try {
			const value = await RegistryFetcher.get( key, propNames );
			return value;
		} catch(err) {
			return undefined;
		}
	}
}

async function getKeyAsObject( key: string ): Promise< RegistryKeyValueDeep >
async function getKeyAsObject( key: string, recurse: false ): Promise< RegistryKeyValueShallow >
async function getKeyAsObject( key: string, recurse: true ): Promise< RegistryKeyValueDeep >

async function getKeyAsObject( key: string, recurse: boolean = true ): Promise< RegistryKeyValue > {
	const regKey = new WinReg(parseKey(key));
	const [ values, keys ]: [RegistryItem[], Registry[]] = await Promise.all( [
		new Promise<RegistryItem[]>( ( resolve: ResolveFunction<RegistryItem[]>, reject: RejectFunction ) => regKey.values( (err: Error|null, result: RegistryItem[]) => err?reject(err):resolve(result) ) ),
		new Promise<Registry[]>( ( resolve: ResolveFunction<Registry[]>, reject: RejectFunction ) => regKey.keys( (err: Error|null, item: Registry[]) => err?reject(err):resolve(item) ) ),
	] );

	return ( await Promise.all( [...(values as RegistryItem[]), ...(keys as Registry[])].map(processItem) as Promise<RegistryFetcherItem>[] ) )
		.filter( x => !!x )
		.sort((a,b) => a.name.localeCompare(b.name))
		.reduce( (R, p) => { R[p.name] = p.value.value; return R;}, {} as RegistryKeyValueObject );

	async function processItem(item: Registry|RegistryItem): Promise< RegistryFetcherItem > {
		const result: RegistryFetcherItem = await (
			item.hasOwnProperty["name"] ? processValueItem(item as RegistryItem) : processKeyItem(item as Registry, key, recurse) );
		result.value.enumerable = true;
		result.value.writable = false;
		return result;
	}

}


function parseKey(fullKey: string): { hive: string, key: string } {
	const parts: string[] = fullKey.split("\\");
	const hive: string = ( (hiveString: string): string => {
		switch(hiveString.toUpperCase()) {
			case "HKEY_CLASS_ROOT":
			case "HKCR":
				return WinReg.HKCR;
			case "HKEY_CURRENT_USER":
			case "HKCU":
				return WinReg.HKCU;
			case "HKEY_LOCAL_MACHINE":
			case "HKLM":
				return WinReg.HKLM;
			case "HKEY_USERS":
			case "HKU":
				return WinReg.HKU;
			case "HKEY_CURRENT_CONFIG":
			case "HKCC":
				return WinReg.HKCC;
			default:
				throw new Error("Invalid registry hive in key path.");
		}
	} )(parts.shift() as string);
	const key: string = ["", ...parts].join("\\");
	return {hive, key};
}


function parseValue(item: RegistryItem ): RegistryValueValue {
	switch(item.type) {
		case "REG_BINARY"       : return item.value;
		case "REG_DWORD"        : return Number.parseInt(item.value, 16);
		case "REG_EXPAND_SZ"    : return item.value;
		case "REG_MULTI_SZ"     : return item.value.split("\\0");
		case "REG_NONE"         : return item.value;
		case "REG_QWORD"        : return Number.parseInt(item.value, 16);
		case "REG_SZ"           : return item.value;
		default                 : return item.value;
	}
}

// async function processKeyItem( item: Registry, key: string, recurse: true ): Promise< RegistryFetcherKeyItemDeep >
// async function processKeyItem( item: Registry, key: string, recurse: false ): Promise< RegistryFetcherKeyItemShallow >
async function processKeyItem( item: Registry, key: string, recurse: boolean ): Promise< RegistryFetcherKeyItem > {
	const childKey: string = item.key.split("\\").pop() as string;
	const fullChildKey = `${key}\\${childKey}`;
	return {
		name: childKey,
		value: {
			configurable: true,
			enumerable: true,
			value: recurse ? ( await getKeyAsObject(fullChildKey) ) : ( async () => getKeyAsObject(fullChildKey, false) ),
			writable: false,
		},
	};
}

function processValueItem(item: RegistryItem ): RegistryFetcherValueItem {
	return {
		name: item.name,
		value: {
			configurable: true,
			enumerable: true,
			value: parseValue(item),
			writable: false,
		},
	};
}