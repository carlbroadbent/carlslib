import { ResolveFunction } from "./types";

export class Semaphore {
	private _available: number;
	private _queue: Array<ResolveFunction>;

	constructor( count: number = 1 ) {
		this._available = count;
		this._queue = [];
	}

	public async acquire(): Promise<void> {
		return new Promise<void>( (resolve: ResolveFunction<void> ) => {
			this._queue.push( resolve );
			this._next();
		} );
	}

	public async exec( task: Function ): Promise<any> {
		await this.acquire();
		try {
			return await task();
		} finally {
			this.release();
		}
	}

	private _next(): void {
		if(this._available && this._queue.length) {
			this._available--;
			const resolve: ResolveFunction = this._queue.shift() as ResolveFunction;
			resolve();
		}
	}

	public release(): void {
		this._available++;
		this._next();
	}
}

export class Mutex extends Semaphore {
	constructor() {
		super( 1 );
	}	
}
