import { Config } from "./Config";


class DebugLogger {

    public static debug(...args:any): void {
        if(Config.debug) {
            Config.logger.debug( ...arguments );
        }
    }

    public static error(...args:any): void {
        Config.logger.error( ...arguments );
    }

    public static info(...args:any): void {
        Config.logger.info( ...arguments );
    }

    public static log(...args:any): void {
        Config.logger.log( ...arguments );
    }

    public static warn(...args:any): void {
        Config.logger.warn( ...arguments );
    }

}

export { DebugLogger };