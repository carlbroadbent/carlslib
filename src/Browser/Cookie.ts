import { TypeTests } from "../TypeTests";

const ATTRIBUTES = [ "DOMAIN", "EXPIRES", "MAX-AGE", "PATH", "SAMESITE", ];
const FLAGS = [ "HTTPONLY", "SECURE", ];

/** @module Cookie */

/**
 * @class
 * @classdesc Simply manage cookie.
 */
class Cookie {

	/**
	 * Erases a value from the cookie, or the whole cookie.
	 *
	 * @param {String} [key] - key to be deleted from cookie.
	 * 			If not provided, all data stored in the cookie will be deleted.
	 */
	static clear( key: string ) {
		if(key) {
			Cookie.set(key, "", { "max-age": 0 } );
		} else {
			Object.keys( Cookie.get() ).forEach( Cookie.clear );
		}
	}

	/**
	 * Gets a value from the cookie, or the whole cookie as an object.
	 *
	 * @param {String} [key] - key for which the value is being retrieved.
	 * 			If not provided, the whole cookie is returned in an object format.
	 * @returns {String|Object} The String value stored in the cookie with the give key
	 * 			or the whole cookie as an Object if no key was provided.
	 */
	static get(): { [ key: string ]: string };
	static get( key: string ): string;
	static get( key?: string ) {
		if(key) {
			return Cookie.get()[key];
		} else {
			return parseCookie();
		}
	}

	/**
	 * Sets a key-value pair in the cookie.  Standard cookie attributes can be defined using the
	 * attributes object.
	 *
	 * @param {String} key - name of variable to be stored
	 * @param {Object} value - value to be stored, non-string values with be toString-ed and stored.
	 * @param {Object} attributes - keys defined as attributes in https://tools.ietf.org/id/draft-ietf-httpbis-rfc6265bis-03.html#rfc.section.5.3
	 * 			Secure and HTTPOnly are flags and carry no value, if the provided value for them resolves to true, the flag will be included.
	 */
	static set( key:string , value:any, attributes?: object ) {
		const encodedKey = encodeURIComponent(key);
		const encodedValue = encodeURIComponent(value);
		const attributesString: string = generateAttributesString(attributes);
		// eslint-disable-next-line no-undef
		document.cookie = `${encodedKey}=${encodedValue};${attributesString}`;
	}

}

/**
 * Parses document.cookie into a javascript Object.  All values will be treated as Strings.
 * All keys and values will have decodeURIComponent run on them we run encodeURIComponent when
 * we set the values.
 *
 * @returns {Object} - Object representation of document.cookie.
 */
function parseCookie(): object {
	// eslint-disable-next-line no-undef
	return document.cookie
	.split( ";")
	.filter( s => s!=="" )
	.map( str => str.split("=").map( s => decodeURIComponent( s.trim() ) ) )
	.reduce( ( R, [k,v] ) => ( { ...R, [k]:v, } ), {} );
}

/**
 * String-encodes the attributes appropriate to them being used to store a cookie value.
 *
 * @param {Object} attributes - keys defined as attributes in https://tools.ietf.org/id/draft-ietf-httpbis-rfc6265bis-03.html#rfc.section.5.3
 * 			Secure and HTTPOnly are flags and carry no value, if the provided value for them resolves to true, the flag will be included.
 */
function generateAttributesString( attributes?: object ): string {
	if(attributes===undefined || TypeTests.getType(attributes)!=="Object") { return ""; }
	return Object.entries(attributes).map( ( [k, v] ) => {
		if(ATTRIBUTES.includes(k.toUpperCase())) { return `${k}=${v}`; }
		if(FLAGS.includes(k.toUpperCase()) && !!v===false ) { return `${k}`; }
		return undefined;
	} ).filter( s => !!s ).join(";");
}

export { Cookie };