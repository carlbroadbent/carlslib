import { Stream } from "stream";
import { fetch, FormData, } from "./lib/fetch";
import { FormattedLogger } from "./FormattedLogger";
import { Streams, StringEncoding } from "./Streams";
import { TypeTests } from "./TypeTests";

/**
 * @typedef {object} GetArguments
 * @property {object}    [headers]
 * @property {object}    [queryParameters]
 * @property {string}    path - a string for the path of the REST endpoint on the server
 */

export interface GetArguments extends URLArguments {
    headers?: {[key:string]: any}
}


/**
* @typedef {object} RequestArguments
* @property {object|NodeJS.ReadableStream|string|undefined} body
* @property {string}        [credentials]
* @property {object}        [headers]
* @property {string}        [method]
* @property {string}        path - a string for the path of the REST endpoint on the server
* @property {object}        [queryParameters]
*/

export interface RequestArguments extends URLArguments {
   body?: object|NodeJS.ReadableStream|string
   credentials?: string
   headers?: {[key:string]: any}
   method?: string
}

/**
* @typedef {object} ApiArguments
* @property {string}     path - a string for the path of the REST endpoint on the server
* @property {object}     queryParameters {string}
*/

export interface URLArguments {
   queryParameters?: {[key:string]: any}
   path: string
}


export class Server {

    public origin: string;
    public machineToken?: string;
    public webToken?: string;

    public constructor( origin: string ) {
        this.origin = origin;
    }

    /**
     * Generate a full URL for the given endpoint on the server.  queryParameters will be serialized and URI encoded.
     *
     * @param {URLArguments} args
     * @returns {string} a formatted URL
     */
    public generateApiUrl( args: URLArguments = { path: "/", queryParameters: {} } ): string {
        const path = args.path || "/";
        const queryParameters = args.queryParameters || {};
        const parameterTuples: Array<[string, any]> = objectEntries( queryParameters );
        const queryString: string = parameterTuples.length===0?"":( "?" +
            parameterTuples
                .map( ( [key, value]: [string, any] ) => {
                    if(typeof value === "object" ) {
                        if(value instanceof Date) {
                            value = value.getTime();
                        } else {
                            value = JSON.stringify(value);
                        }
                    }
                    return [ key, value ].map( encodeURIComponent ).join("=");
                } ).join( "&" )
        );
        return `${this.origin}${path}${queryString}`;
    }

    public async get( args = {} as GetArguments ) {
        return this.request( { ...args, method: "GET", } );
    }

    public async post( args = {} as RequestArguments ) {
        return this.request( {...args, method: "POST", } );
    }

    public async request( args: RequestArguments): Promise<NodeJS.ReadableStream | null | object | ReadableStream<Uint8Array> | string | {[key:string]: any}>  {
        args.credentials = "same-origin";
        args.headers ??= {};
        args.method ??= args.body ? "POST" : "GET";
        args.path ??= "/";
        args.queryParameters ??= {};

        if( TypeTests.isObject(args.body) ) {
            if( TypeTests.isReadableStream(args.body) === false ) {
                args.headers["Content-Type"] = "application/json";
                console.log(args.headers);
                args.body = JSON.stringify( args.body );
            }
        }

        let token = args.path.split("/")[1]==="desktop" ? this.machineToken : this.webToken

        if(token) {
            // The current state of token authentication in the API.
            // Some endpoints use the "token" value in the Cookie header.
            // Some endpoints use the "webtoken" get parameter.
            //
            // It looks like Tanner put together a middleware to authenticate through the X-Auth-Token heaeder.

            // Cookie Auth Method
            const cookieJar: {[key:string]:any} = args.headers.hasOwnProperty("Cookie") ? args.headers["Cookie"].split(";").map( (s:string)  => s.trim().split("=") ).reduce( ( R: object, [k,v]: [string,string] ) => ( {...R, [k]:v} ), {} ) : {};
            cookieJar.webtoken = token;
            cookieJar.token = token;
            args.headers.Cookie = Object.entries(cookieJar).map( ( a: Array<string> ) => a.join("=") ).join("; ");

            // Query Parameter Method - THIS SEEMS TO BE MOST PREVALENT
            args.queryParameters.webtoken = args.queryParameters.webtoken || token;

            // X-Auth-Token Auth Method
            args.headers["X-Auth-Token"] = token;

            // WE SHOULD IMPLEMENT A Bearer Token AUTHENTICATION MIDDLEWARE ON THE SERVER AND GET RID OF THE OTHERS.
            // (https://tools.ietf.org/html/rfc6750)
        }

        // Format Request
        FormattedLogger.log(args.body);
        const url = this.generateApiUrl( args );
        const { path, queryParameters, ...requestParameters} = args;

        FormattedLogger.log("fetch(%j, %j)", url, requestParameters);


        // Send Request
        const response = await fetch( url, requestParameters as object );

        // Process Response
        if(response.status >= 300 ) {
            throw new Error( response.statusText );
        }

        let contentType : null|string|undefined = response.headers.get("content-type");
        if(contentType) {
            contentType = contentType.split(";").shift() as string;

            if( contentType.startsWith("text/")  || ["application/javascript", "application/json"].includes(contentType) ) {
                const bodyString: string = await Streams.toString( response.body as Stream );
                switch( contentType ) {
                    case "application/json":
                        return JSON.parse(bodyString);
                    default:
                        return bodyString;
                }
            }
        }
        return response.body;
    }

}

function objectEntries( obj: {[key:string]:any} = {} ): Array<[string, any]> {
    return Object.entries(obj) || []; // ECMAScript 2017
    // return Object.keys(obj).map<[string, any]>( ( key ) => [ key, obj[key] ] );
}