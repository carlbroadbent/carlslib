// @ts-nocheck
import { EventEmitter } from "events";
import { getCodeLocation } from "./Functions";
import { fName } from "./Functions/fName";
import { FormattedLogger } from "./FormattedLogger";


const BUILTIN_TYPES = [ "Arguments", "AggregateError", "Array", "ArrayBuffer",
	"AsyncFunction", "AsyncIterator", "Atomics", "BigInt", "BigInt64Array",
	"BigUint64Array", "Boolean", "DataView", "Date", "Error", "EvalError",
	"Float32Array", "Flat64Array", "Function", "Generator", "GeneratorFunction",
	"Int8Array", "Int16Array", "Int32Array", "InternalError", "Intl",
	//"Intl.Collator", "Intl.DateTimeFormat", "Intl.ListFormat", "Intl.Locale", "Intl.NumberFormat", "Intl.PluralRules", "Intl.RelativeTimeFormat",
	"Iterator", "JSON", "Math", "Number", "Null", "Object", "Promise", "Proxy",
	"RangeError", "ReferenceError", "Reflect", "RegExp", "SharedArrayBuffer",
	"String", "SyntaxError", "TypeError", "Uint8Array", "Uin8ClampedArray",
	"Uint16Array", "Undefined", "URIError", "WebAssembly",
	// "WebAssembly.CompileError", "WebAssembly.Instance", "WebAssembly.LinkError", "WebAssembly.Memory", "WebAssembly.Module", "WebAssembly.RuntimeError", "WebAssembly.Table",
];

const TESTABLE_TYPES =  [...BUILTIN_TYPES, "ReadableStream", "Stream", "WritableStream" ].sort();

const LOWERCASE_TYPES =  TESTABLE_TYPES.map( s => s.toLowerCase() );

/** @module TypeTests */
class TypeTests {
	/**
     *
     * @param {*} obj object to be tested
     */
	static getType( obj ) {
		FormattedLogger.debug( `${fName(true)}(${new Array(arguments.length).fill("%j").join(", ")})`, ...arguments );
		let type = Object.prototype.toString.call(obj).slice(8, -1);
		if(type === "Object") { type = getSpecialObjectTypes( obj ); }
		return type;
	}

	/**
	 * 
	 * @param {*} obj
	 */
	static isReadableStream(obj) {
		FormattedLogger.debug( `${fName(true)}(${new Array(arguments.length).fill("%j").join(", ")})`, ...arguments );
		return obj instanceof EventEmitter && TypeTests.getType(obj.read)==="Function";
	}

	/**
	 * @param {*} obj
	 */
	static isStream(obj) {
		FormattedLogger.debug( `${fName(true)}(${new Array(arguments.length).fill("%j").join(", ")})`, ...arguments );
		return TypeTests.isReadableStream(obj) || TypeTests.isWritableStream(obj);
	}

	/**
     *
     * @param {*} obj variable to test
     * @param {string|string[]} type string value representing variable type, or array representing multiple types
     */
	static isType(obj, type) {
		FormattedLogger.debug( `${fName(true)}(${new Array(arguments.length).fill("%j").join(", ")})`, ...arguments );
		if( Array.isArray(type) ) {
			return type.some( (t) => TypeTests.isType(obj, t) );
		} else if( type==="Stream" ) {
			return TypeTests.isStream(obj);
		} else if(TypeTests.getType(type)==="String") {
			const objType = TypeTests.getType( obj );
			const lcType = type.toLowerCase();
			const lcObjType = objType.toLowerCase();
			if( LOWERCASE_TYPES.includes( lcType ) ) {
				return lcType===lcObjType;
			} else {
				return lcType===lcObjtype || obj.constructor.name.toLowerCase()===lcType;	
			}
		}
		throw new Error( `non-string type name: ${type}` );
	}

	/**
	 * @param {*} obj - variable to test
	 */
	static isWritableStream(obj) {
		FormattedLogger.debug( `${fName(true)}(${new Array(arguments.length).fill("%j").join(", ")})`, ...arguments );
		return obj instanceof EventEmitter && TypeTests.isType(obj.write, "Function") && TypeTests.isType(obj.end, "Function");
	}

	/**
	 *
	 * @param {*} obj - variable to test
	 * @param {string|string[]} type String value representing variable type
	 * @param {string} vName formatted variable name to be printed in error message
	 */
	static validateType( obj, type, vName ) {
		return privateValidateType( obj, type, vName, 2 );
	}

}

BUILTIN_TYPES.forEach ( ( typeName ) => {
	FormattedLogger.debug( `${fName(true)}(${new Array(arguments.length).fill("%j").join(", ")})`, ...arguments );
	const modifiedTypeName = typeName.split(".").join("");
	TypeTests[`is${modifiedTypeName}`] = function(obj) { return TypeTests.isType( obj, typeName); }
} );

TESTABLE_TYPES.forEach ( ( typeName ) => {
	FormattedLogger.debug( `${fName(true)}(${new Array(arguments.length).fill("%j").join(", ")})`, ...arguments );
	const modifiedTypeName = typeName.split(".").join("");
	TypeTests[`validate${modifiedTypeName}`] = function(obj, vName) { return validateAllNamedTypes( obj, vName); }
} );

/**
 * 
 * @param {*} obj 
 */
function getSpecialObjectTypes(obj) {
	FormattedLogger.debug( `${fName(true)}(${new Array(arguments.length).fill("%j").join(", ")})`, ...arguments );
	let type = "Object";
	const isReadableStream = TypeTests.isReadableStream(obj);
	if( isReadableStream ) { type = "ReadableStream"; }
	if( TypeTests.isWritableStream(obj) ) { type = isReadableStream ? "Stream" : "WritableStream"; }
	return type;
}

/**
 * 
 * @param {*} obj 
 * @param {*} vName 
 */
function validateAllNamedTypes( obj, vName) {
	FormattedLogger.debug( `${fName(true)}(${new Array(arguments.length).fill("%j").join(", ")})`, ...arguments );
	// console.log(getCodeLocation(1));
	return privateValidateType( obj, getCodeLocation(1).functionName.substr(8), vName, 3);
}

/**
 * 
 * @param {*} obj 
 * @param {*} type 
 * @param {*} vName 
 * @param {*} lookback 
 */
function privateValidateType( obj, type, vName = "Parameter", lookback = 2 ) {
	FormattedLogger.debug( `${fName(true)}(${new Array(arguments.length).fill("%j").join(", ")})`, ...arguments );
	if(
		(
			TypeTests.getType(type)==="String" ||
			(Array.isArray(type) && type.every( t => TypeTests.getType(t)==="String" ) )
		) === false
	) {
		throw new Error( `${fName(true, 0)}: variable type, found ${TypeTests.getType(type)}, expected String|String[].`);
	}
	if( !TypeTests.isString(vName) ) {
		throw new Error( `${fName(true, 0)}: variable vName, found ${TypeTests.getType(vName)}, expected String.`);
	}
	if( !TypeTests.isNumber(lookback) ) {
		throw new Error( `${fName(true, 0)}: variable lookback, found ${TypeTests.getType(lookback)}, expected Number.`);
	}
	if( !TypeTests.isType(obj, type) ) {
		const eLoc = getCodeLocation(lookback);
		const locationString = eLoc.file ? `${eLoc.file}:${eLoc.line}` : `${eLoc.functionName}:${eLoc.line}`; 
		throw new Error( `${locationString} found ${vName} of type ${TypeTests.getType(obj)}, expected ${Array.isArray(type)?type.join(", "):type}.`);
	}
}

export { TypeTests };