"use strict";

import { Stats } from "fs";
import { RejectFunction, ResolveFunction } from "./types";

/** @module FileSystem */

import FS from "fs";
import OS from "os";
import Path from "path";

import { fName } from  "./Functions" ;
import { Logging } from "./Logging";
import { Streams, StringEncoding } from "./Streams";

type FilePermission = { gid: number, group: AccessPermission, other: AccessPermission, uid: number, user: AccessPermission };
type AccessPermission = { read: boolean, write: boolean, execute: boolean, }

/**
 * @class
 * @classdesc A class with static functions for specific filesystem related functionality.
 */
export class FileSystem {

	/**
     * Makes a unixified Windows path appear as a properly formatted windows path.
     * @public
     *
	 * @function deunixifyPath
     * @param {String} fullPath
     */
	static deunixifyPath( fullPath: string ): string {
		Logging.debug( `${fName(true)}(%j)`, ...arguments );
		return process.platform==="win32"
			? fullPath.substr(1).replace(/\//g, "\\")
			: fullPath
		;
	}

	/**
     * Checks if a file exists at the given path.
     * @public
     *
	 * @async
	 * @function exists
     * @param {String} path to test
	 * @returns {Promise<Boolean, Error>} A promise that resolves a boolean telling if a file exists at the given path.
	 * If an underlying error was thrown, that will be rejeced instead.
     */
	static async exists(path: string): Promise<Boolean> {
		Logging.debug( `${fName(true)}(%j)`, ...arguments );
		try {
			await FS.promises.lstat( path );
			return true;
		} catch(err) {
			if(( err as NodeJS.ErrnoException ).code == "ENOENT") { return false; }
			throw err;
		}
	}

	/**
     * Test whether a file or folder at the given path has a certain permission flag set for the specified uid or gid.
     * @public
     *
	 * @async
	 * @function hasPermission
     * @param {Object} obj
     * @param {String} obj.gid
     * @param {String} obj.path
     * @param {String} obj.permissionName
     * @param {String} obj.uid
	 * @returns {Promise<Boolean, Error>}
     */
	static async hasPermission(obj: { gid?: number|string, path: string, permissionName: "execute"|"read"|"write", uid?: number|string } ) {
		Logging.debug( `${fName(true)}(%j)`, ...arguments );
		let permissions = await FileSystem.permissions(obj.path);
		return ( permissions.other[obj.permissionName] )
            || ( permissions.group[obj.permissionName] && Number(obj.gid)==permissions.gid )
            || ( permissions.user [obj.permissionName] && Number(obj.uid)==permissions.uid )
		;
	}

	/**
     * Checks if a the given path is a folder.
     * @public
     *
     * @param {String} path
     */
	static async isDirectory(path:string): Promise<boolean> {
		Logging.debug( `${fName(true)}(%j)`, ...arguments );
		const stats = await FS.promises.lstat( path );
		return stats.isDirectory();
	}

	/**
     * Checks if a the given path is a regular file.
     * @public
     *
     * @param {String} path
     */
	static async isFile(path:string): Promise<boolean> {
		Logging.debug( `${fName(true)}(%j)`, ...arguments );
		const stats = await FS.promises.lstat( path );
		return stats.isFile();
	}

	/**
     * Test whether a file or folder at the given path is readable by the specified uid or gid.
     * @public
     *
     * @param {Object} obj
     * @param {String} obj.gid
     * @param {String} obj.path
     * @param {String} obj.uid
     */
	static async isReadable( obj: { gid?: number|string, path:string, uid?: number|string } ): Promise<boolean> {
		Logging.debug( `${fName(true)}(%j)`, ...arguments );
		return FileSystem.hasPermission( { permissionName: "read", ... obj } );
	}

	/**
     * Checks if a the given path is a folder.
     * @public
     *
     * @param {String} path
     */
	static async isSymbolicLink(path: string): Promise<boolean> {
		Logging.debug( `${fName(true)}(%j)`, ...arguments );
		try {
			const stats = await FS.promises.lstat( path );
			return stats.isSymbolicLink();
		} catch(err) {
			if(( err as NodeJS.ErrnoException ).code==="NOENT") { return false; }
			throw err;
		}
	}

	/**
     * Test whether a file or folder at the given path is readable by the specified uid.
     * @public
     *
     * @param {String} path
     * @param {String} userId
     */
	static async isUserReadable( path: string, userId: number|string ): Promise<boolean> {
		Logging.debug( `${fName(true)}(%j, %j)`, ...arguments );
		return FileSystem.isReadable( { path: path, uid: userId } );
	}

	/**
     * Test whether a file or folder at the given path is writable by the specified uid.
     * @public
     *
     * @param {String} path
     * @param {String} userId
     */
	static async isUserWritable( path:string, userId: number|string): Promise<boolean> {
		Logging.debug( `${fName(true)}(%j, %j)`, ...arguments );
		return FileSystem.isWritable( { path: path, uid: userId } );
	}

	/**
     * Test whether a file or folder at the given path is writable by the specified uid or gid.
     * @public
     *
     * @param {Object} obj
     * @param {String} obj.gid
     * @param {String} obj.path
     * @param {String} obj.uid
     */
	static async isWritable( obj: { gid?: number|string, path:string, uid?: number|string } ): Promise<boolean> {
		Logging.debug( `${fName(true)}(%j)`, ...arguments );
		return FileSystem.hasPermission( { permissionName: "write", ... obj } );
	}

	/**
     * Get permissions of the file at the given path.
     * @public
     *
     * @param {String} path
     */
	static async permissions(path: string ): Promise<FilePermission> {
		Logging.debug( `${fName(true)}(%j)`, ...arguments );
		const stats = await FS.promises.lstat( path );
		return {
			gid     : stats.gid,
			uid     : stats.uid,
			user    : {
				read    : !!(stats.mode & 0o400),
				write   : !!(stats.mode & 0o200),
				execute : !!(stats.mode & 0o100),
			},
			group   : {
				read    : !!(stats.mode & 0o040),
				write   : !!(stats.mode & 0o020),
				execute : !!(stats.mode & 0o010),
			},
			other   : {
				read    : !!(stats.mode & 0o004),
				write   : !!(stats.mode & 0o002),
				execute : !!(stats.mode & 0o001),
			},
		};
	}

	static async readFileToString(path: string, encoding: StringEncoding): Promise<string> {
		Logging.debug( `${fName(true)}(%j, %j)`, ...arguments );
		return Streams.toString(FS.createReadStream(path), encoding);
	}

	/**
     * Changes leading "~", "%HOMEPATH%" and "%USERPROFILE%" in a path to the User's home directory.
     * @public
     *
     * @param {String} path
     */
	static resolveHome(path: string): string {
		Logging.debug( `${fName(true)}(%j)`, ...arguments );
		let parts = path.split(/[\\/]/);
		if( [ "~", "%HOMEPATH%", "%USERPROFILE%", ].includes( parts[0].toUpperCase() ) ) {
			parts.shift();
			parts = [ ...OS.homedir().split(/[\\/]/), ...parts, ];
		}
		return ( OS.platform()==="win32" ? "" : Path.sep ) + Path.join( ...parts );
	}

	/**
     * Writes the contents of a readable stream to a file at the given path.
     *
     * @param {*} readStream
     * @param {String} filePath
     */

	static async saveStreamToFile( readStream: NodeJS.ReadableStream, filePath: string ): Promise<boolean> {
		Logging.debug( `${fName(true)}(%j, %j)`, ...arguments );
		const writeStream = FS.createWriteStream( filePath, { autoClose: true, emitClose: true, } );
		readStream.pipe(writeStream);
		return new Promise<boolean>( (resolve: ResolveFunction<boolean>, reject: RejectFunction) => {
			writeStream.on("error", reject );
			writeStream.on("finish", () => resolve(true) );
		} );
	}

	/**
     *
     * @param {String} string
     * @param {String} filePath
     */

	static async saveStringToFile(string: string, filePath: string): Promise<boolean> {
		Logging.debug( `${fName(true)}(%j, %j)`, ...arguments );
		return FileSystem.saveStreamToFile( Streams.fromString(string), filePath);
	}


	/**
     * When run on windows, takes a properly formatted Windows path and converts it to
     * a unix-style path with forward slashes.
     * @public
     *
     * @param {String} filePath
     */
	static unixifyPath( filePath:string ): string {
		Logging.debug( `${fName(true)}(%j)`, ...arguments );
		return process.platform==="win32"
			? `/${filePath.replace(/\\/g, "/")}`
			: filePath
		;
	}

}