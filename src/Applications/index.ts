import Darwin from "./darwin";
import Other from "./other";
import Win32 from "./win32";

const Applications = ( () => {
    switch(process.platform) {
        case "darwin" : return Darwin;
        case "win32"  : return Win32;
        default       : return Other;
    }
} )();

export { Applications };