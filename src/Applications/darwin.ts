import Path from "path";
import Plist from "plist";
import { execute } from "../Functions";

import { Application } from "./Application";

/** @module Applications */

export = class Applications {

	/**
	 * Checks if the Application is installed on the local machine.
	 *
	 * @async
	 * @function installed
	 * @param {String} name Name of program being searched for
	 * @returns {Promise.<Boolean, Error>} A promise
	 */
	static async installed( name: string ): Promise<boolean> {
		return Object.values(await Applications.list())
			.some( (app: Application, ) => [ app.name, app.path, ].includes( name ) );
	}

	static async list(): Promise<{ [key:string]: Application }>  {
		const XML: string = await execute( "system_profiler -xml SPApplicationsDataType" ) as string;
		const xmlArray: Plist.PlistArray = Plist.parse(XML) as Plist.PlistArray;
		const xmlObject: Plist.PlistObject = xmlArray[0] as Plist.PlistObject;
		const items: Plist.PlistObject[] = xmlObject._items as Plist.PlistObject[];
		return items.map( (item) => {
			return {
				arch: resolveArch(item.arch_kind as string),
				displayName: item._name,
				folderPath: Path.dirname(item.path as string),
				installDate: new Date(item.lastModified as string),
				name: item._name,
				path: item.path,
				version: item.version,
			} as Application;
		} )
		.reduce( (R, item) => { R[item.name] = item; return R; }, {} as { [key:string]: Application } );
	}

	static async openFile(fileName: string, appName: string): Promise<boolean|string> {
		return appName
			? execute( `open -a "${appName}" "${fileName}"`, true )
			: execute( `open "${fileName}"`, true )
		;
	}
}

function resolveArch(archString:string): ("arm64"|"x64"|"x86")[] {
	switch(archString) {
		case "arch_arm": return ["arm64"];
		case "arch_arm_i64": return ["arm64", "x64"];
		case "arch_i32": return ["x86"];
		case "arch_i32_i64": return ["x64", "x86"];
		case "arch_i64": return ["x64"];
		default: return [];
	}
}