export type Application = {
	arch: ("arm32"|"arm64"|"x64"|"x86")[];
	displayName: string;
	folderPath: string;
	installDate: Date;
	name: string;
	path: string;
	version: string;
}
