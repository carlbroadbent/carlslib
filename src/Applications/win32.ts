import Path from "path";

import { RegistryFetcher } from "../RegistryFetcher";
import { execute, } from "../Functions" ;

import { Application } from "./Application";

const ARCH = [ "x86", "x64", ];

const REG_KEYS = [
	"HKLM\\Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall",
	"HKLM\\Software\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall",
];

export = class Applications {

	static async installed( name: string ): Promise<boolean> {
		const ucName = name.toUpperCase();
		return Object.values(await Applications.list())
		.some( (app) => [ app.name.toUpperCase(), app.path.toUpperCase(), ].includes(ucName) );
	}

	static async list(): Promise<{ [ key: string ]: Application }> {
		const rawList = ( await Promise.all( REG_KEYS.map(RegistryFetcher.getDeep) ) )
		.map( (regKey, i, ) => {
			return Object.entries(regKey as object).map( ( [ k, v ]: any[] ) => {
				v.regKey = k;
				v.arch = ARCH[i];
				return v;
			} );
		} )
		.reduce( ( R, L, ) => [ ...R, ...(L?L:[]), ] , [] )
		.filter( o => !!o.DisplayName )
		.map( (obj) => {
			let folderPath = obj.InstallLocation;
			let name = undefined;
			let path = undefined;

			if( !obj.regKey.match(/^\{[0-9A-F]{4}(?:[0-9A-F]{4}-){4}[0-9A-F]{12}\}$/i) ) { name = obj.regKey; }
			if(obj.DisplayIcon) {
				let matches = undefined;
				if(obj.DisplayIcon) { matches = obj.DisplayIcon.match(/^(.*\\([^\\]*)\.exe)(?:,\d+)?$/); }
				if(matches) {
					path = matches[1];
					folderPath = Path.dirname(path);
					if( !name && matches[2]!=="" ) { name = matches[2]; }
				}
			}
			if(!name) { name = obj.DisplayName; }
			let installDate = undefined;
			if(obj.installDate) { installDate = new Date(Date.parse(obj.InstallDate.match(/^(\d{4})(\d\d)(\d\d)$/).splice(1).join("-"), )+ (new Date().getTimezoneOffset())*60*1000 ); }
			return {
				arch        : [obj.arch as "x64"|"x86"],
				displayName : obj.DisplayName,
				folderPath,
				installDate,
				name,
				path,
				version     : obj.DisplayVersion,
			} as Application;
		} )
		.filter( o => !!o.path )
		.reduce( (R, obj: Application) => { R[obj.name] = obj; return R; }, {} as { [ key: string ]: Application } )
        ;

		return rawList;
	}

	static async openFile(fileName: string, appName: string): Promise<boolean|string> {
		const ucName = appName.toUpperCase();
		const appList = await Applications.list();
		const truncatedList = Object.values(appList).filter( app => [app.name.toUpperCase(), app.path.toUpperCase()].includes(ucName) );
		if(truncatedList.length) { appName = truncatedList[0].path; }
		return appName
			? execute( `start "" "${appName}" "${fileName}"`, true, )
			: execute( `start "" "${fileName}"`, true, )
		;
	}
};