"use strict";

import Util from "util";
import { DebugLogger } from "./DebugLogger";
import { fName } from "./Functions/fName";
import { Strings } from "./Strings";

const LOG_LEVELS:Array<String> = [ "debug", "error", "info", "log", "warn", ];
const LEVEL_LENGTH:number = LOG_LEVELS.reduce( (R, logLevel) => Math.max(R,logLevel.length), 0);

class FormattedLogger extends DebugLogger {
	public static debug( ...args: any[] ): void { return super.debug( ...formatArgs(...args) ); }
	public static error( ...args: any[] ): void { return super.error( ...formatArgs(...args) ); }
	public static info(  ...args: any[] ): void { return super.info(  ...formatArgs(...args) ); }
	public static log(   ...args: any[] ): void { return super.log(   ...formatArgs(...args) ); }
	public static warn(  ...args: any[] ): void { return super.warn(  ...formatArgs(...args) ); }
}

function formatArgs(...args: any[]): string[] {
	const values = [
		"%s [%s] %s : ",
		new Date().toISOString(),
		Strings.appendSpacesToLength(fName(false, 1), LEVEL_LENGTH),
		fName(true, 2),
	];
	
	if( args && args.length>0 ) {
		values[0] += "%s";
		if( Object.prototype.toString.call(args[0]) === "[object String]" ) {
			values.push( Util.format( args[0], ...(args.slice(1)) ) ); 
		} else {
			values.push( ...args );
		}
	}
	return values;
}

export { FormattedLogger };