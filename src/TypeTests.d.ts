export declare  type TestableType =  "Arguments" | "AggregateError" | "Array" | "ArrayBuffer" | "AsyncFunction" |
	"AsyncIterator" | "Atomics" | "BigInt" | "BigInt64Array" | "BigUint64Array" | "Boolean" | "DataView" |
	"Date" | "Error" | "EvalError" | "Float32Array" | "Flat64Array" | "Function" | "Generator" |
	"GeneratorFunction" | "Int8Array" | "Int16Array" | "Int32Array" | "InternalError" | "Intl" |
	"Iterator" | "JSON" | "Math" | "Number" | "Null" | "Object" | "Promise" | "Proxy" | "RangeError" |
	"ReadableStream" | "ReferenceError" | "Reflect" | "RegExp" | "SharedArrayBuffer" | "Stream" |
	"String" | "SyntaxError" | "TypeError" | "Uint8Array" | "Uin8ClampedArray" | "Uint16Array" |
	"Undefined" | "URIError" | "WebAssembly" | "WritableStream";

/** @class TypeTests */
export declare class TypeTests {
	public static getType( obj: any ): string;
	public static isArguments( obj: any ): boolean;
	public static isAggregateError( obj: any ): boolean;
	public static isArray( obj: any ): boolean;
	public static isArrayBuffer( obj: any ): boolean;
	public static isAsyncFunction( obj: any ): boolean;
	public static isAsyncIterator( obj: any ): boolean;
	public static isAtomics( obj: any ): boolean;
	public static isBigInt( obj: any ): boolean;
	public static isBigInt64Array( obj: any ): boolean;
	public static isBigUint64Array( obj: any ): boolean;
	public static isBoolean( obj: any ): boolean;
	public static isDataView( obj: any ): boolean;
	public static isDate( obj: any ): boolean;
	public static isError( obj: any ): boolean;
	public static isEvalError( obj: any ): boolean;
	public static isFloat32Array( obj: any ): boolean;
	public static isFlat64Array( obj: any ): boolean;
	public static isFunction( obj: any ): boolean;
	public static isGenerator( obj: any ): boolean;
	public static isGeneratorFunction( obj: any ): boolean;
	public static isInt8Array( obj: any ): boolean;
	public static isInt16Array( obj: any ): boolean;
	public static isInt32Array( obj: any ): boolean;
	public static isInternalError( obj: any ): boolean;
	public static isIntl( obj: any ): boolean;
	public static isIterator( obj: any ): boolean;
	public static isJSON( obj: any ): boolean;
	public static isMath( obj: any ): boolean;
	public static isNumber( obj: any ): boolean;
	public static isNull( obj: any ): boolean;
	public static isObject( obj: any ): boolean;
	public static isPromise( obj: any ): boolean;
	public static isProxy( obj: any ): boolean;
	public static isRangeError( obj: any ): boolean;
	public static isReadableStream( obj: any ): boolean;
	public static isReferenceError( obj: any ): boolean;
	public static isReflect( obj: any ): boolean;
	public static isRegExp( obj: any ): boolean;
	public static isSharedArrayBuffer( obj: any ): boolean;
	public static isStream( obj: any ): boolean;
	public static isString( obj: any ): boolean;
	public static isSyntaxError( obj: any ): boolean;
	public static isType( obj: any, type: TestableType | TestableType[] ): boolean;
	public static isTypeError( obj: any ): boolean;
	public static isUint8Array( obj: any ): boolean;
	public static isUin8ClampedArray( obj: any ): boolean;
	public static isUint16Array( obj: any ): boolean;
	public static isUndefined( obj: any ): boolean;
	public static isURIError( obj: any ): boolean;
	public static isWebAssembly( obj: any ): boolean;
	public static isWritableStream( obj:any ): boolean;
	public static validateArguments( obj: any, vName?: string ): void;
	public static validateAggregateError( obj: any, vName?: string ): void;
	public static validateArray( obj: any, vName?: string ): void;
	public static validateArrayBuffer( obj: any, vName?: string ): void;
	public static validateAsyncFunction( obj: any, vName?: string ): void;
	public static validateAsyncIterator( obj: any, vName?: string ): void;
	public static validateAtomics( obj: any, vName?: string ): void;
	public static validateBigInt( obj: any, vName?: string ): void;
	public static validateBigInt64Array( obj: any, vName?: string ): void;
	public static validateBigUint64Array( obj: any, vName?: string ): void;
	public static validateBoolean( obj: any, vName?: string ): void;
	public static validateDataView( obj: any, vName?: string ): void;
	public static validateDate( obj: any, vName?: string ): void;
	public static validateError( obj: any, vName?: string ): void;
	public static validateEvalError( obj: any, vName?: string ): void;
	public static validateFloat32Array( obj: any, vName?: string ): void;
	public static validateFlat64Array( obj: any, vName?: string ): void;
	public static validateFunction( obj: any, vName?: string ): void;
	public static validateGenerator( obj: any, vName?: string ): void;
	public static validateGeneratorFunction( obj: any, vName?: string ): void;
	public static validateInt8Array( obj: any, vName?: string ): void;
	public static validateInt16Array( obj: any, vName?: string ): void;
	public static validateInt32Array( obj: any, vName?: string ): void;
	public static validateInternalError( obj: any, vName?: string ): void;
	public static validateIntl( obj: any, vName?: string ): void;
	public static validateIterator( obj: any, vName?: string ): void;
	public static validateJSON( obj: any, vName?: string ): void;
	public static validateMath( obj: any, vName?: string ): void;
	public static validateNumber( obj: any, vName?: string ): void;
	public static validateNull( obj: any, vName?: string ): void;
	public static validateObject( obj: any, vName?: string ): void;
	public static validatePromise( obj: any, vName?: string ): void;
	public static validateProxy( obj: any, vName?: string ): void;
	public static validateRangeError( obj: any, vName?: string ): void;
	public static validateReadableStream( obj: any, vName?: string ): void;
	public static validateReferenceError( obj: any, vName?: string ): void;
	public static validateReflect( obj: any, vName?: string ): void;
	public static validateRegExp( obj: any, vName?: string ): void;
	public static validateSharedArrayBuffer( obj: any, vName?: string ): void;
	public static validateStream( obj: any, vName?: string ): void;
	public static validateString( obj: any, vName?: string ): void;
	public static validateSyntaxError( obj: any, vName?: string ): void;
	public static validateType( obj: any, type: TestableType | TestableType[], vName?: string ): void;
	public static validateTypeError( obj: any, vName?: string ): void;
	public static validateUint8Array( obj: any, vName?: string ): void;
	public static validateUint8ClampedArray( obj: any, vName?: string ): void;
	public static validateUint16Array( obj: any, vName?: string ): void;
	public static validateUndefined( obj: any, vName?: string ): void;
	public static validateURIError( obj: any, vName?: string ): void;
	public static validateWebAssembly( obj: any, vName?: string ): void;
	public static validateWritableStream( obj: any, vName?: string ): void;
}