class Objects {
	/**
	 * Remove a given property from an object.
	 *
	 * @param {string} key key to be removed from the object
	 * @param {Object} obj Object to remove property from
	 * @returns {Object} a new object with the same values as obj, but with the specified properties removed.
	 */
	// eslint-disable-next-line no-unused-vars
	static removeKey( key: number|string, { [key]:_, ...obj } ) {
		return obj;
	}

	/**
	 * Remove specified properties from an object.
	 *
	 * @param {Array<string>} keys keys to be removed from the object
	 * @param {Object} obj Object to remove property from
	 * @returns {Object} a new object with the same values as obj, but with the specified properties removed.
	 */
	static removeKeys(keys: (number|string)[], obj:object) {
		return keys.reduce( (R, key) => Objects.removeKey(key, R), obj );
	}
}

// Objects.removeKey = (k = "", { [k]:_, ...o } = {}) => o;
// Objects.removeKeys = (keys = [], o = {}) => keys .reduce ((r, k) => Objects.removeKey (k, r), o);

export { Objects };