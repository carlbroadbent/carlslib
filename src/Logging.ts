"use strict";

import Util from "util";
import { Config } from "./Config";
import { fName } from "./Functions/fName";
import { Strings } from "./Strings";

const LOG_LEVELS:Array<String> = [ "debug", "error", "info", "log", "verbose", "warn", ];
const LEVEL_LENGTH:number = LOG_LEVELS.reduce( (R, logLevel) => Math.max(R,logLevel.length), 0);

/** @module Logging */
const Logging = LOG_LEVELS.reduce(
	(R:any, logLevel:any) => ( {...R, [logLevel as string]: generateGenericLoggingFunction(logLevel as string) } ),
	{ debug: false, console },
) ;

export { Logging };

function generateGenericLoggingFunction( logLevel: string ): Function {
	return function (...args: any[] ) {
		const DEBUG = Logging.debug || Config.debug;
		const Console = Logging.console;
		if(logLevel!=="debug" || DEBUG) {
			const values = [
				new Date().toISOString(),
				Strings.appendSpacesToLength(logLevel, LEVEL_LENGTH),
				// fName(true, 1),
			];
			if(args.length) {
				values.push( Util.format( args[0], ...args.slice(1) ) );
			}
			// const pattern = `%s [%s] %s : ${args.length?"%s":""}`;
			const pattern = `%s [%s] : ${args.length?"%s":""}`;
			Console[logLevel]( pattern, ...values );
		}
	};
}
