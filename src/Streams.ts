"use strict";

/** @module Streams */
import Stream from "stream";
import { FileSystem } from "./FileSystem";
import { fName } from "./Functions";
import { FormattedLogger } from "./FormattedLogger";
import { ResolveFunction } from "./types";
import { TypeTests } from "./TypeTests";

export type StringEncoding = "utf16le" | "utf8" | "ascii" | "utf-8" | "ucs2" | "ucs-2" | "base64" | "latin1" | "binary" | "hex" | undefined;

export class Streams {

	static fromString(string: string): NodeJS.ReadableStream {
		FormattedLogger.debug( `${fName(true)}(%j)`, ...Array.from(arguments) );
		return new Stream.Readable( { read() { this.push(string); this.push(null); } } );
	}

	static async saveStreamToFile( readStream: NodeJS.ReadableStream, filePath: string ): Promise<boolean> {
		FormattedLogger.debug( `${fName(true)}(%j, %j)`, ...arguments );
		return FileSystem.saveStreamToFile( readStream, filePath );
	}

	static async toString(stream: Stream, encoding?: StringEncoding|undefined ): Promise<string> {
		FormattedLogger.debug( `${fName(true)}(%j, %j)`, TypeTests.getType(stream), encoding );
		const chunks: Array<any> = [];
		stream.on( "data", ( chunk : Buffer) => {
			if(encoding===undefined) {
				/* Showing my ignorance here.
                 * If encoding is undefined, we can determine if it"s UTF-16 Little Endian based on
                 * Byte Order Marking (BOM).  UTF-16 LE will lead with 0xfffe and UTF-16 BE would
                 * lead with 0xfeff.  Big endian isn"t an option in javascript, so we"ll just check
                 * if the stream is UTF16-LE encoded, otherwise, we"ll default to utf8.  If we do use
                 * the UTF-16 encoding, we need to remove those two leading bytes.
                 */
				if(chunk[0]==255 && chunk[1]==254) {
					encoding = "utf16le";
				} else if( encoding===undefined ) {
					encoding = "utf8";
				} else {
					chunk = chunk.slice(2);
				}
			}
			chunks.push( chunk );
		} );
		return new Promise<string>( ( resolve: ResolveFunction<string> ): void => {
			stream.on( "end",  () => resolve( Buffer.concat(chunks).toString(encoding) ) );
		} );
	}
}