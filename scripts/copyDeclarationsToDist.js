const FS = require( "fs" );
const Path = require( "path" );

( async function main(){
    const srcPath = Path.resolve( __dirname, "..", "src" );
    const declPath = Path.resolve( __dirname, "..", await getDeclDir() );
    const paths = await listDeclarations( srcPath ).then( paths => paths.map( path => path.slice(srcPath.length) ) );
    for( const path of paths ) {
        try {
            await FS.promises.mkdir( Path.join( declPath, Path.dirname(path) ), true );
        } catch( e ) {
            if( e.code !== "EEXIST" ) {
                throw e;
            }
        }
        console.log("Copying", path.substr(1) );
        await FS.promises.copyFile( Path.join(srcPath, path), Path.join(declPath, path) );
    }
} )();

async function listDeclarations( path ) {
    const stats = await FS.promises.stat( path );
    if( stats.isDirectory() ) {
        const results = [];
        const files = await FS.promises.readdir( path );
        for( const fileName of files ) {
            results.push( ... await listDeclarations( Path.join( path, fileName ) ) );
        }
        return results;
    }
    if( path.endsWith(".d.ts") ) {
        return [ path ];
    }
    return [];
}

async function getDeclDir() {
    const obj = await readJSONWithComments( Path.resolve( __dirname, "..", "tsconfig.json") );
    return obj.compilerOptions.declarationDir;
}

async function readJSONWithComments( path ) {
    const inBuf = await FS.promises.readFile( path );
    let outBuf = "";
    let doubleSlash = false;
    let escape = false;
    let inQuotes = false;
    let previous = undefined;
    let slashStar = false;
    for(const n of inBuf) {
        const c = String.fromCharCode(n);
        if( ! inQuotes ) {
            if( !( doubleSlash || slashStar) ) {
                if( previous==="/" ) {
                    if( c==="/" ) {
                        doubleSlash = true;
                    } else if( c==="*" ) {
                        slashStar = true;
                    } else {
                        outBuf += previous;
                    }
                }
                if( c === "\"" ) {
                    inQuotes = true;
                }
            }
        } else {
            if( c==="\"" ) {
                inQuotes = escape;
            }
            if( c==="\\" ) {
                escape = ! escape;
            } else {
                escape = false;
            }
        }

        if( ! ( doubleSlash || slashStar ) && ( c!=="/" || inQuotes ) ) {
            outBuf += c;
        }

        if( doubleSlash && c==="\n" ) {
            doubleSlash = false;
        }

        if( slashStar && previous==="*" && c==="/" ) {
            slashStar = false;
            previous = undefined;
        } else {
            previous = c;
        }

    }
    if( previous==="/" && ! ( doubleSlash || slashStar ) ) {
        outBuf += previous;
    }
    return JSON.parse( outBuf );
}